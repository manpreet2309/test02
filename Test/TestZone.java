import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestZone {
Zone c = new Zone();

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
//R1 A on way trip inside zone 1
	@Test
	public void testOneWayTrip() {
		String[] f = new String[]{"leslie"};
		String[] t = new String[]{"Don Mills"};
		
		double charges =c .calculateTotal(f,t);
		assertEquals(2.50,charges,0.0);
		 
		
	}
	
	//R1 A on way trip inside zone 2
		@Test
		public void testOneWayTrip2() {
			String[] f = new String[]{"Sheppard"};
			String[] t = new String[]{"Finch Station"};
			
			double charges =c .calculateTotal(f,t);
			assertEquals(3.00,charges,0.0);
			 
			
		}


}
